import pytest  # noqa: F401
from django.urls import resolve, reverse

from .views import home


def test_home_view_status_code(client):
    url = reverse("core:home")
    response = client.get(url)
    assert response.status_code == 200


def test_home_url_resolves_home_view():
    view = resolve("/")
    assert view.func.__name__ == home.__name__
