# docker django postgres template

[![GitHub license](https://img.shields.io/badge/license-MIT-green)](LICENSE)

This template is for django. The django project is contained within docker and is ready for development. The django project already has a non-modified [custom user model](https://docs.djangoproject.com/en/3.2/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project). It is set up for use with PostgreSQL which is also included as a docker container. An example production deployment is included, which includes NGINX, and Redis.

I have also created and included several [alternative custom user models](/www/django/src/templates_django/templates_django/account/alternative%20user%20models), and included them in this project:

- Login with an email address

  - [with a username field](/www/django/src/templates_django/templates_django/account/alternative%20user%20models/Log%20in%20with%20email%20address/Username%20field)
  - [without a username field](/www/django/src/templates_django/templates_django/account/alternative%20user%20models/Log%20in%20with%20email%20address/No%20username%20field)

- [Username is no longer case sensitive](/www/django/src/templates_django/templates_django/account/alternative%20user%20models/Username%20is%20not%20case%20sensitive)


## Getting Started
### Requirements
- [Docker](https://docs.docker.com/engine/install/)

Start by cloning the project:

```sh
$ git clone https://gitlab.com/emilyjt/docker-django-and-postgres-template.git <your-project>
$ cd <your-project>
```

I have included an [initialisation script](django_init.py) (written in Python) that: generates new secrets, and renames the project files automatically.

Run the script:

```sh
$ docker run -it --rm --name init-django -v ${PWD}:/usr/src/myapp -w /usr/src/myapp python:3 python django_init.py
```

Which will generate an output similar to below:

```sh
Unable to find image 'python:3' locally
3: Pulling from library/python
0ecb575e629c: Pull complete
7467d1831b69: Pull complete
feab2c490a3c: Pull complete
f15a0f46f8c3: Pull complete
937782447ff6: Pull complete
e78b7aaaab2c: Pull complete
b68a1c52a41c: Pull complete
ddcd772f47ec: Pull complete
0753beeb7344: Pull complete
Digest: sha256:942bc4201d0fe995d18dcf8ca50745cfe3d16c253f54366af10cae18a2bfe7f6
Status: Downloaded newer image for python:3

Please enter either: a project name, or the domain name this project will be deployed to:
$ example.co.uk
9 files have been updated, and 2 directories have been renamed.
```

The project is now ready for development.

### I want a local virtual environment for black/flake8/etc...
#### Poetry

```sh
$ poetry install
$ poetry shell
```

#### venv

```sh
$ python -m venv .venv
$ ./.venv/Scripts/activate
(venv) $ pip install -r ./www/django/requirements.development.txt
```

### pre-commit

[pre-commit](https://pre-commit.com/) is a Python package that automatically performs a [list of tasks](.pre-commit-config.yaml) before a git commit. It will be installed when you create a virtual envrionment and install the development requirements.

Activating it is simple:

```sh
$ pre-commit install
```

If you wish to force pre-commit to run, without needing to perform a git commit, you can do so:

```sh
$ pre-commit run --all-files
Trim Trailing Whitespace.................................................Passed
Fix End of Files.........................................................Passed
Check Yaml...............................................................Passed
black....................................................................Passed
isort....................................................................Passed
flake8...................................................................Passed
```

## Useful commands

To start the development server:

```sh
$ docker-compose -f docker-compose.dev.yml up -d
```

To stop the development server:

```sh
$ docker-compose -f docker-compose.dev.yml down
```

To run a command using django `manage.py`:

(On the development server, the container name is extrapolated from the project name/domain name entered on my initialisation script - which is the SERVICENAME var in .env)

```sh
$ docker exec example_django python manage.py <command>
```

To get a permanent shell inside the container:

```sh
$ docker exec -it example_django /bin/sh -c "[ -e /bin/bash ] && /bin/bash || /bin/sh"
```

## Running tests

To run tests I usually bind my local code directly to a docker container, so it updates dynamically and then run pytest inside the container.
I therefore run the following command to get me a bash terminal inside a django container:

```sh
$ docker-compose -f docker-compose.dev.yml run --rm django bash
```

I can then run pytest at will, make changes to my code, and run pytest again:

```sh
root@d6084a146748:/app/django# pytest
======================================= test session starts =======================================
platform linux -- Python 3.9.2, pytest-6.2.2, py-1.10.0, pluggy-0.13.1
django: settings: config.settings.test (from ini)
rootdir: /app/django, configfile: pytest.ini
plugins: django-4.1.0
collected 2 items

exampleproj/core/tests.py ..                                                                 [100%]

======================================== 2 passed in 0.55s ========================================
```

## Authors

- [**emilyjt**](https://gitlab.com/emilyjt/)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

- spookylukey - [Django Views — The Right Way](https://spookylukey.github.io/django-views-the-right-way/the-pattern.html)
- [Cookiecutter Django](https://github.com/pydanny/cookiecutter-django)
- Claudio Jolowicz - [Hypermodern Python](https://cjolowicz.github.io/posts/hypermodern-python-01-setup/)
- Vitor Freitas - [How to Use Django's Built-in Login System](https://simpleisbetterthancomplex.com/tutorial/2016/06/27/how-to-use-djangos-built-in-login-system.html)
- [calmcode](https://calmcode.io/)
